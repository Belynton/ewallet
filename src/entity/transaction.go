package entity

import (
	"EWallet/src/rest"
	"errors"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"time"
)

type Transaction struct {
	TransactionId   string    `json:"transaction_id"`
	FromId          string    `json:"from_id"`
	ToId            string    `json:"to_id"`
	TransactionTime time.Time `json:"transaction_time"`
	Amount          float64   `json:"amount"`
}

func (transaction Transaction) GetLastTransactions(db *sqlx.DB, number int) ([]Transaction, error) {
	query := "select transaction_id as TransactionId, from_id as FromId, to_id as ToId, transaction_time as TransactionTime, amount as Amount " +
		"from wallet_transaction order by transaction_time DESC limit $1"
	var transactions []Transaction
	if err := db.Select(&transactions, query, number); err != nil {
		return nil, err
	}
	return transactions, nil
}

func (transaction Transaction) Create(db *sqlx.DB, dto rest.SendDto) error {
	var wallet Wallet
	if err := db.Get(&wallet, "select * from wallet where id=$1", dto.From); err != nil {
		return err
	}
	if !isTransactionValid(wallet.Amount, dto.Amount) {
		return errors.New("bad amount for transaction")
	}
	tx := db.MustBegin()
	tx.MustExec("update wallet set amount = amount - $1 where id = $2", dto.Amount, dto.From)
	tx.MustExec("update wallet set amount = amount + $1 where id = $2", dto.Amount, dto.To)
	tx.MustExec("insert into wallet_transaction(transaction_id, from_id, to_id, transaction_time, amount) values ($1,$2,$3,$4,$5)",
		uuid.NewString(), dto.From, dto.To, time.Now(), dto.Amount)
	return tx.Commit()
}

func (transaction Transaction) CreateTable(db *sqlx.DB) error {
	_, err := db.Exec("create table wallet_transaction(" +
		"transaction_id uuid not null primary key, " +
		"from_id uuid, " +
		"to_id uuid, " +
		"transaction_time timestamp, " +
		"amount numeric, " +
		"FOREIGN KEY (from_id) REFERENCES wallet (id), " +
		"FOREIGN KEY (to_id) REFERENCES wallet (id) " +
		")")
	return err
}

func isTransactionValid(walletAmount, transactionAmount float64) bool {
	return (transactionAmount > 0) && (walletAmount-transactionAmount) >= 0
}
