package entity

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type Wallet struct {
	Id     string  `json:"id"`
	Amount float64 `json:"amount"`
}

func (wallet Wallet) GetWalletById(db *sqlx.DB, id string) (*Wallet, error) {
	err := db.Get(&wallet, "select * from wallet WHERE id=$1", id)
	return &wallet, err
}

func (wallet Wallet) GetWallets(db *sqlx.DB) ([]Wallet, error) {
	var wallets []Wallet
	err := db.Select(&wallets, "select * from wallet")
	return wallets, err
}

func (wallet Wallet) CreateTable(db *sqlx.DB) error {
	_, err := db.Exec("create table wallet(id uuid not null primary key, amount numeric)")
	return err
}

func (wallet Wallet) FillTable(db *sqlx.DB) error {
	_, err := db.Exec("insert into wallet (id, amount) values ($1, $2)", uuid.NewString(), 100.0)
	return err
}
