package main

import (
	"EWallet/src/entity"
	"EWallet/src/rest"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
	"net/http"
	"strconv"
	"time"
)

const (
	host     = "ewallet_db"
	port     = "5432"
	user     = "ewallet"
	password = "ewallet"
	db       = "ewallet"
)

var database *sqlx.DB

func main() {
	connectDatabase(5)
	defer database.Close()
	initWallets()
	router := gin.Default()
	router.POST("/api/send", sendMoney)
	router.GET("/api/wallet/:id/balance", getBalance)
	router.GET("/api/transactions", getLast)
	router.GET("/api/wallets", getWallets) // добавил для просмотра кошельков без подключения к бд через приложения типа dbeaver
	router.Run("0.0.0.0:8080")
}

func connectDatabase(maxAttempts int8) {
	connStr := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		user, password, host, port, db)
	for maxAttempts > 0 {
		time.Sleep(5 * time.Second)
		db, err := sqlx.Connect("pgx", connStr)
		if err != nil {
			fmt.Printf("Failed to connect: %s %s\n", err, time.Now())
		} else {
			database = db
			return
		}
		maxAttempts -= 1
	}
}

func initWallets() {
	var wallet entity.Wallet
	var transaction entity.Transaction
	if err := database.Get(&wallet, "select * from wallet limit 1"); err == nil {
		return
	}
	wallet.CreateTable(database)
	for i := 0; i < 10; i++ {
		wallet.FillTable(database)
	}
	transaction.CreateTable(database)
}

func sendMoney(c *gin.Context) {
	var sendDto rest.SendDto
	var transaction entity.Transaction
	if err := c.BindJSON(&sendDto); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "Bad parameters request"})
		return
	}
	if err := transaction.Create(database, sendDto); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	c.IndentedJSON(http.StatusCreated, gin.H{"message": "Transaction completed"})
}

func getWallets(c *gin.Context) {
	var wallet entity.Wallet
	val, err := wallet.GetWallets(database)
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "Bad request"})
		return
	}
	c.IndentedJSON(http.StatusOK, val)
}

func getLast(c *gin.Context) {
	number, err := strconv.Atoi(c.Query("number"))
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "Bad request"})
		return
	}
	var transaction entity.Transaction
	val, err2 := transaction.GetLastTransactions(database, number)
	if err2 != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "Bad request"})
		return
	}
	c.IndentedJSON(http.StatusOK, val)
}

func getBalance(c *gin.Context) {
	id := c.Param("id")
	var wallet entity.Wallet
	val, err := wallet.GetWalletById(database, id)
	if err != nil {
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Wallet not found"})
		return
	}
	c.IndentedJSON(http.StatusOK, val)
}
